/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import solitude.common.entities.Entity;
import solitude.common.entities.EntityDirection;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.entities.entity_instances.EntityDeadTree;
import solitude.common.entities.entity_instances.EntityNightBush;
import solitude.common.entities.entity_instances.EntityNightTree;
import solitude.common.entities.entity_instances.EntityNightTree2;
import solitude.common.entities.entity_instances.EntityShadowDeadTree;
import solitude.common.entities.entity_instances.EntityShadowNightBush;
import solitude.common.entities.entity_instances.EntityShadowNightTree;
import solitude.common.entities.entity_instances.EntitySmallStone;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.entities.entity_instances.WorldObject;
import solitude.common.game_data.Background;
import solitude.common.game_data.Fog;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameKeys;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;
import solitude.common.provided_interfaces.InfcEntityUpdateService;
import solitude.common.statistics.HeadsUpDisplay;
import solitude.common.statistics.Statistics;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class GameScreen implements ApplicationListener {

    private Lookup.Result<InfcEntityInstantiateService> result;
    private final Lookup lookup = Lookup.getDefault();
    private List<InfcEntityInstantiateService> instantiatePlugins = Collections.synchronizedList(new ArrayList());

    private GameData gamedata;
    private GameWorld gameworld;

    private OrthographicCamera camera;

    private SpriteBatch batch;
    private ShapeRenderer shape;

    private float screenY;
    private float screenX;

    private float delta;

    private boolean game_over = false;

    //Saving memory space, by initializing those variables before create.
    private double health = 1;
    private double healthBar;

    @Override
    public void create() {
        gamedata = new GameData();
        gameworld = new GameWorld();
        batch = new SpriteBatch();
        shape = new ShapeRenderer();

        gamedata.setDisplayWidth(Gdx.graphics.getWidth());
        gamedata.setDisplayHeight(Gdx.graphics.getHeight());

        camera = new OrthographicCamera(((gamedata.getDisplayWidth() / 2) / 1.5f), ((gamedata.getDisplayHeight() / 2) / 1.5f));
        camera.translate(((gamedata.getDisplayWidth() / 2) / 1.5f), ((gamedata.getDisplayHeight() / 2) / 1.5f));
        camera.update();

        screenY = (((gamedata.getDisplayHeight() / 2) / 1.5f) / 2);
        screenX = (((gamedata.getDisplayWidth() / 2) / 1.5f) / 2);

        Gdx.input.setInputProcessor(new GameInput(gamedata));

        result = lookup.lookupResult(InfcEntityInstantiateService.class);
        result.addLookupListener(lookupListener);
        result.allItems();

        for (InfcEntityInstantiateService plugin : result.allInstances()) {
            plugin.create(gameworld, gamedata);
            instantiatePlugins.add(plugin);
        }
    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        gamedata.setDelta(Gdx.graphics.getDeltaTime());
        delta += gamedata.getDelta();
        gamedata.getKeys().update();

        TextureLoader.loadRenderingMaterial(gameworld);

        if (gameworld.getSprites().isEmpty()) {
            updateCameraPosition();
            update();
            drawBackground();
            drawWorldContext();
            drawShader();
            drawLife_and_progress_bars();
            drawHeadsUpDisplay();
            drawStats();
            GameState();
            gameOver();
        }

        if (gamedata.getKeys().isDown(GameKeys.SHIFT)) {
            testCollisionAndHitBoxes();
        }
    }

    private void GameState() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        if (gameworld.getAmountOfEnemies() <= 0) {
            String string = "YOU WIN";
            TextureLoader.getFont().draw(batch, string, camera.position.x - screenX + 170, camera.position.y - screenY + 135);
        }
        batch.end();
    }

    private void drawHeadsUpDisplay() {
        camera.update();
        for (HeadsUpDisplay hud : gameworld.getHeadsUpDisplay()) {
            batch.setProjectionMatrix(camera.combined);
            batch.begin();
            if (TextureLoader.bar_frame != null) {
                batch.draw(TextureLoader.bar_frame, camera.position.x - screenX + hud.getX(), camera.position.y - screenY + hud.getY());
            }
            batch.end();
        }
    }

    private void drawLife_and_progress_bars() {
        camera.update();
        if (!gameworld.getHeadsUpDisplay().isEmpty()) {
            shape.setProjectionMatrix(camera.combined);

            for (Entity useful_entity : gameworld.getEntities()) {
                if (useful_entity instanceof PlayerEntity) {
                    health = ((PlayerEntity) useful_entity).getHealth();
                }
            }

            if (((health / 350) * 141) <= 0) {
                healthBar = 0;
                game_over = true;
            } else {
                healthBar = (health / 350) * 141;
            }
            double progressBar = ((-141 * gameworld.getAmountOfEnemies()) / GameWorld.AMOUNT_OF_GUARDS) + 141;
            shape.begin(ShapeType.Filled);
            shape.setColor(1, 0, 0, 1);
            shape.rect(camera.position.x - screenX + 72, camera.position.y - screenY + 12, (float) healthBar, 16);
            shape.setColor(1, 0, 1, 0);
            shape.rect(camera.position.x - screenX + 216, camera.position.y - screenY + 12, (float) progressBar, 16);
            shape.end();
        }

    }

    private void gameOver() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        if (game_over) {
            String string = "YOU DIED";
            TextureLoader.getFont().draw(batch, string, camera.position.x - screenX + 170, camera.position.y - screenY + 135);
        }
        batch.end();
    }

    private void testCollisionAndHitBoxes() {
        ShapeRenderer hitbox = new ShapeRenderer();
        hitbox.setAutoShapeType(true);
        hitbox.setProjectionMatrix(camera.combined);
        hitbox.begin(ShapeRenderer.ShapeType.Line);
        for (Entity e : gameworld.getEntities()) {
            if (e instanceof PlayerEntity) {
                hitbox.setColor(0, 0, 255, 1);
                hitbox.circle(((PlayerEntity) e).getHitboxx(), ((PlayerEntity) e).getHitboxy(), ((PlayerEntity) e).getHitboxRadius());
                hitbox.setColor(255, 0, 0, 1);
                hitbox.circle(((PlayerEntity) e).getCollideX(), ((PlayerEntity) e).getCollideY(), ((PlayerEntity) e).getRadiusBound());
            }
            if (e instanceof WorldObject) {
                hitbox.setColor(255, 0, 0, 1);
                hitbox.circle(((WorldObject) e).getCollideX(), ((WorldObject) e).getCollideY(), ((WorldObject) e).getRadiusBound());
            }
            if (e instanceof EnemyEntity) {
                hitbox.setColor(0, 0, 255, 1);
                hitbox.circle(((EnemyEntity) e).getHitboxx(), ((EnemyEntity) e).getHitboxy(), ((EnemyEntity) e).getHitboxRadius());
            }
        }
        hitbox.end();
    }

    private void drawBackground() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        for (Background b : gameworld.getBackgrounds()) {
            if (TextureLoader.night_grass != null) {
                batch.draw(TextureLoader.night_grass, b.getX(), b.getY());
            }
        }
        batch.end();
    }

    private void drawWorldContext() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        for (int i = 0; i <= gameworld.getSortetListOfEntities().size() - 1; i++) {
            Entity e = gameworld.getSortetListOfEntities().get(i);

            if (e.getX() < (screenX) + camera.position.x
                    && e.getX() > (-screenX) + camera.position.x) {
                if (e.getY() < (screenY + 10) + camera.position.y
                        && e.getY() > (-screenY - 30) + camera.position.y) {

                    if (e instanceof PlayerEntity) {
                        if (TextureLoader.playerLoaded()) {
                            if (((PlayerEntity) e).isIdle()) {
                                batch.draw(drawFlippedTexture(e, TextureLoader.player_idle),
                                        e.getX(), e.getY());
                            } else if (((PlayerEntity) e).isWalking()) {
                                batch.draw(drawFlippedTexture(e, TextureLoader.player_walking),
                                        e.getX(), e.getY());
                            } else if (((PlayerEntity) e).isAttacking()) {
                                Animation attack = TextureLoader.getRightWeapon(((PlayerEntity) e));
                                batch.draw(drawFlippedTexture(e, attack),
                                        e.getX(), e.getY());
                            }
                        }
                    }

                    if (e instanceof EnemyEntity) {
                        if (TextureLoader.enemyLoaded()) {
                            if (TextureLoader.night_guard_attacking != null && TextureLoader.night_guard_walking != null) {
                                if (((EnemyEntity) e).isAttacking()) {
                                    batch.draw(drawFlippedTexture(e, TextureLoader.night_guard_attacking),
                                            e.getX(), e.getY());
                                } else {
                                    batch.draw(drawFlippedTexture(e, TextureLoader.night_guard_walking),
                                            e.getX(), e.getY());
                                }
                            }
                        }
                    }

                    if (e instanceof EntityNightTree) {
                        if (TextureLoader.night_tree != null) {
                            batch.draw(TextureLoader.night_tree, e.getX(), e.getY());
                        }
                    }
                    if (e instanceof EntityNightTree2) {
                        if (TextureLoader.night_tree_2 != null) {
                            batch.draw(TextureLoader.night_tree_2, e.getX(), e.getY());
                        }
                    }
                    if (e instanceof EntityDeadTree) {
                        if (TextureLoader.dead_tree != null) {
                            batch.draw(TextureLoader.dead_tree, e.getX(), e.getY());
                        }
                    }
                    if (e instanceof EntityNightBush) {
                        if (TextureLoader.night_bush != null) {
                            batch.draw(TextureLoader.night_bush, e.getX(), e.getY());
                        }
                    }
                    if (e instanceof EntitySmallStone) {
                        if (TextureLoader.small_stone != null) {
                            batch.draw(TextureLoader.small_stone, e.getX(), e.getY());
                        }
                    }

                    if (e instanceof EntityShadowNightBush) {
                        if (TextureLoader.night_bush_shadow != null) {
                            batch.draw(TextureLoader.night_bush_shadow, e.getX(), e.getY());
                        }
                    }
                    if (e instanceof EntityShadowNightTree) {
                        if (TextureLoader.night_tree_shadow != null) {
                            batch.draw(TextureLoader.night_tree_shadow, e.getX(), e.getY());
                        }
                    }
                    if (e instanceof EntityShadowDeadTree) {
                        if (TextureLoader.dead_tree_shadow != null) {
                            batch.draw(TextureLoader.dead_tree_shadow, e.getX(), e.getY());
                        }
                    }
                }
            }
        }
        batch.end();

    }

    private TextureRegion drawFlippedTexture(Entity e, Animation component_animation) {
        TextureRegion texture = component_animation.getKeyFrame(delta, true);

        if (e.getDirection().equals(EntityDirection.RIGHT)) {
            if (texture.isFlipX()) {
                texture.flip(true, false);
            }
        }
        if (e.getDirection().equals(EntityDirection.LEFT)) {
            if (!texture.isFlipX()) {
                texture.flip(true, false);
            }
        }

        return texture;
    }

    private void drawShader() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        if (!gameworld.getFog().isEmpty()) {
            for (Fog s : gameworld.getFog()) {
                if (TextureLoader.fog_of_war != null) {
                    batch.draw(TextureLoader.fog_of_war, camera.position.x - screenX, camera.position.y - screenY);
                }
            }
        }
        batch.end();
    }

    private void drawStats() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        for (Statistics s : gameworld.getStatistics()) {
            String string = s.getTitle() + s.getCounter();
            TextureLoader.getFont().draw(batch, string, camera.position.x - screenX + s.getX(), camera.position.y - screenY + s.getY());
        }
        batch.end();
    }

    private void updateCameraPosition() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        for (Entity e : gameworld.getEntities()) {
            if (e instanceof PlayerEntity) {
                float cameraX = e.getX() + (e.getSizex() / 2);
                float cameraY = e.getY() + (e.getSizey() / 2);

                float screenXR = -screenX + (gameworld.getWORLD_SIZE() * gameworld.getWORLD_SCALE());
                float screenYH = -screenY + (gameworld.getWORLD_SIZE() * gameworld.getWORLD_SCALE());

                if (cameraX >= screenX && cameraX <= screenXR) {
                    camera.position.x = cameraX;
                } else {
                    if (cameraX <= (gameworld.getWORLD_SIZE() * gameworld.getWORLD_SCALE()) / 2) {
                        camera.position.x = screenX;
                    } else {
                        camera.position.x = screenXR;
                    }
                }

                if (cameraY >= screenY && cameraY <= screenYH) {
                    camera.position.y = cameraY;
                } else {
                    if (cameraY <= (gameworld.getWORLD_SIZE() * gameworld.getWORLD_SCALE()) / 2) {
                        camera.position.y = screenY;
                    } else {
                        camera.position.y = screenYH;
                    }
                }
                camera.update();
            }
        }
    }

    private void update() {
        for (InfcEntityUpdateService entityProcessorService : getEntityProcessingServices()) {
            entityProcessorService.update(gameworld, gamedata);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    private Collection<? extends InfcEntityUpdateService> getEntityProcessingServices() {
        return lookup.lookupAll(InfcEntityUpdateService.class);
    }

    private final LookupListener lookupListener = new LookupListener() {

        @Override
        public synchronized void resultChanged(LookupEvent le) {

            Collection<? extends InfcEntityInstantiateService> updated = result.allInstances();

            for (InfcEntityInstantiateService us : updated) {

                // Newly installed modules
                if (!instantiatePlugins.contains(us)) {
                    us.create(gameworld, gamedata);
                    instantiatePlugins.add(us);
                }
            }

            // Stop and remove module
            synchronized (instantiatePlugins) {
                for (InfcEntityInstantiateService gs : instantiatePlugins) {
                    if (!updated.contains(gs)) {
                        gs.dispose(gameworld, gamedata);
                        instantiatePlugins.remove(gs);
                        Gdx.gl.glClearColor(1, 1, 1, 1);
                        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
                        camera.update();
                    }
                }
            }
        }
    };

}
