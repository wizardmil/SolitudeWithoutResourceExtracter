/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.GameWorld;
import static solitude.core.ComponentAnimator.ComponentAnimator;

/**
 *
 * @author Bruger
 */
public class TextureLoader {

    private GameWorld gameworld;

    private static TextureRegion night_guard_walking_sheet;
    private static TextureRegion night_guard_attack_sheet;
    public static Animation night_guard_walking;
    public static Animation night_guard_attacking;

    public static Animation player_walking;
    public static Animation player_idle;

    public static Animation player_attacking_1;
    public static Animation player_attacking_2;
    public static Animation player_attacking_3;

    public static TextureRegion night_grass;

    public static TextureRegion night_tree;
    public static TextureRegion night_tree_2;
    public static TextureRegion dead_tree;
    public static TextureRegion night_bush;
    public static TextureRegion small_stone;

    public static TextureRegion dead_tree_shadow;
    public static TextureRegion night_bush_shadow;
    public static TextureRegion night_tree_shadow;

    public static TextureRegion fog_of_war;

    public static TextureRegion bar_frame;

    public static BitmapFont font;

    private static boolean enemy = false;

    private static boolean player = false;

    public TextureLoader(GameWorld gameworld) {
        this.gameworld = gameworld;
    }

    public static void loadRenderingMaterial(GameWorld gameworld) {
        if (!gameworld.getSprites().isEmpty()) {
            for (TexturePath sheet : gameworld.getSprites()) {
                /**
                 * 1*
                 */
                if (sheet.getType().equals(TextureType.PLAYER_IDLE)) {
                    player_idle = ComponentAnimator(loadTexture(sheet.getModule(), sheet.getResource()), 2, 5, 3f);
                    System.out.println("Texture animation: " + player_idle + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 2*
                 */
                if (sheet.getType().equals(TextureType.PLAYER_WALKING)) {
                    player_walking = ComponentAnimator(loadTexture(sheet.getModule(), sheet.getResource()), 2, 3, 8f);
                    System.out.println("Texture animation: " + player_walking + " " + sheet.getType());
                    player = true;
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 3*
                 */
                if (sheet.getType().equals(TextureType.PLAYER_ATTACK_1)) {
                    player_attacking_1 = ComponentAnimator(loadTexture(sheet.getModule(), sheet.getResource()), 2, 2, 8f);
                    System.out.println("Texture animation: " + player_attacking_1 + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 4*
                 */
                if (sheet.getType().equals(TextureType.PLAYER_ATTACK_2)) {
                    player_attacking_2 = ComponentAnimator(loadTexture(sheet.getModule(), sheet.getResource()), 2, 2, 8f);
                    System.out.println("Texture animation: " + player_attacking_2 + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 5*
                 */
                if (sheet.getType().equals(TextureType.ENEMY_IDLE)) {
                    night_guard_walking = ComponentAnimator(loadTexture(sheet.getModule(), sheet.getResource()), 1, 4, 6f);
                    System.out.println("Texture animation: " + night_guard_walking + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 6*
                 */
                if (sheet.getType().equals(TextureType.ENEMY_ATTACKING)) {
                    night_guard_attacking = ComponentAnimator(loadTexture(sheet.getModule(), sheet.getResource()), 1, 3, 8f);
                    System.out.println("Texture animation: " + night_guard_attacking + " " + sheet.getType());
                    enemy = true;
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 7*
                 */
                if (sheet.getType().equals(TextureType.HUD)) {
                    bar_frame = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + bar_frame + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 8*
                 */
                if (sheet.getType().equals(TextureType.FOG_OG_WAR)) {
                    fog_of_war = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + fog_of_war + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 9*
                 */
                if (sheet.getType().equals(TextureType.BACKGROUND)) {
                    night_grass = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + night_grass + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 10*
                 */
                if (sheet.getType().equals(TextureType.NIGHT_TREE)) {
                    night_tree = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + night_tree + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 11*
                 */
                if (sheet.getType().equals(TextureType.NIGHT_TREE_2)) {
                    night_tree_2 = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + night_tree_2 + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 12*
                 */
                if (sheet.getType().equals(TextureType.DEAD_TREE)) {
                    dead_tree = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + dead_tree + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 13*
                 */
                if (sheet.getType().equals(TextureType.NIGHT_BUSH)) {
                    night_bush = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + night_bush + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 14*
                 */
                if (sheet.getType().equals(TextureType.SMALL_STONE)) {
                    small_stone = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + small_stone + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 15*
                 */
                if (sheet.getType().equals(TextureType.SHADOW_TREE_2)) {
                    dead_tree_shadow = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + dead_tree_shadow + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 16*
                 */
                if (sheet.getType().equals(TextureType.SHADOW_BUSH)) {
                    night_bush_shadow = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + night_bush_shadow + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
                /**
                 * 17*
                 */
                if (sheet.getType().equals(TextureType.SHADOW_TREE_1)) {
                    night_tree_shadow = new TextureRegion(loadTexture(sheet.getModule(), sheet.getResource()));
                    System.out.println("Texture animation: " + night_tree_shadow + " " + sheet.getType());
                    gameworld.removeSprite(sheet);
                }
            }
        }

    }
    
    public static BitmapFont getFont(){
        if(font == null) {
            font = new BitmapFont(Gdx.files.internal("font/8bit.fnt"), Gdx.files.internal("font/8bit.png"), false);
            return font;
        } else {
            return font;
        }
    }

    public static boolean enemyLoaded() {
        if (enemy) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean playerLoaded() {
        if (player) {
            return true;
        } else {
            return false;
        }
    }

    public static Animation getRightWeapon(PlayerEntity e) {
        if (e.getWeapon().getName().contains("standard")) {
            return player_attacking_1;
        } else if (e.getWeapon().getName().contains("rage")) {
            if (player_attacking_2 != null) {
                return player_attacking_2;
            } else {
                return player_attacking_1;
            }
        } else {
            return player_idle;
        }
    }

    private static Texture loadTexture(Class cls, String path) {
        byte[] data;
        Pixmap pixmap;
        Texture temp_texture = null;
        try {
            data = loadAsByteArray(cls, path);
            pixmap = new Pixmap(data, 0, data.length);
            temp_texture = new Texture(pixmap);
            pixmap.dispose();
            System.out.println("Returned texture : " + temp_texture.getHeight());
            return temp_texture;
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
        return temp_texture;
    }

    private static byte[] loadAsByteArray(Class cls, String path) throws IOException, NullPointerException {
        final byte[] data;

        try (InputStream is = cls.getClassLoader().getResourceAsStream(path)) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = is.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }

            data = baos.toByteArray();

            baos.close();
            is.close();
        }

        return data;
    }

}
