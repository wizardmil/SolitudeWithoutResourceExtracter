/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.shadows;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.EntityDeadTree;
import solitude.common.entities.entity_instances.EntityNightBush;
import solitude.common.entities.entity_instances.EntityNightTree;
import solitude.common.entities.entity_instances.EntityNightTree2;
import solitude.common.entities.entity_instances.EntityShadowDeadTree;
import solitude.common.entities.entity_instances.EntityShadowNightBush;
import solitude.common.entities.entity_instances.EntityShadowNightTree;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */

@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)

public class ShadowUpdater implements InfcEntityUpdateService{

    private boolean loaded = false; 
    
    @Override
    public void update(GameWorld world, GameData data) {
        if(!loaded){
            setShadows(world);
            loaded = true;
        }
    }
    
    private void setShadows(GameWorld world){
        TexturePath night_bush_sprite = new TexturePath("shadows/night_bush_shadow.png", ShadowUpdater.class, TextureType.SHADOW_BUSH);
        world.addSprite(night_bush_sprite);
        TexturePath night_tree_sprite = new TexturePath("shadows/blue_tree_shadow.png", ShadowUpdater.class, TextureType.SHADOW_TREE_1);
        world.addSprite(night_tree_sprite);
        TexturePath night_tree_dead_sprite = new TexturePath("shadows/dead_tree_shadow.png", ShadowUpdater.class, TextureType.SHADOW_TREE_2);
        world.addSprite(night_tree_dead_sprite);
        
        for(Entity w : world.getEntities()){
            if(w instanceof EntityNightBush){
                Entity shadow = new EntityShadowNightBush();
                shadow.setX(w.getX());
                shadow.setY(w.getY() + 0.1f);
                world.addEntity(shadow);
            }
            if(w instanceof EntityNightTree || w instanceof EntityNightTree2){
                Entity shadow = new EntityShadowNightTree();
                shadow.setX(w.getX() - 20);
                shadow.setY(w.getY() + 0.1f);
                world.addEntity(shadow);
            }
            if(w instanceof EntityDeadTree){
                Entity shadow = new EntityShadowDeadTree();
                shadow.setX(w.getX() - 20);
                shadow.setY(w.getY() + 0.1f);
                world.addEntity(shadow);
            }
        }
    }
    
}
