/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.shadows;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.EntityShadowDeadTree;
import solitude.common.entities.entity_instances.EntityShadowNightBush;
import solitude.common.entities.entity_instances.EntityShadowNightTree;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;

/**
 *
 * @author Bruger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})

public class ShadowDisposer implements InfcEntityInstantiateService {

    @Override
    public void create(GameWorld gameworld, GameData gamedata) {

    }

    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
        if (!gameworld.getEntities().isEmpty()) {
            for (Entity s : gameworld.getEntities()) {
                if (s instanceof EntityShadowNightBush) {
                    gameworld.removeEntity(s);
                }
                if (s instanceof EntityShadowNightTree) {
                    gameworld.removeEntity(s);
                }
                if (s instanceof EntityShadowDeadTree) {
                    gameworld.removeEntity(s);
                }
            }
        }
    }

}
