package solitude.forrestofsolitude;

//import solitude.common.provided_interfaces.InfcEntityInstantiateService;
//import solitude.common.provided_interfaces.InfcEntityUpdateService;
import java.io.IOException;
import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import junit.framework.Test;
import static junit.framework.TestCase.assertEquals;
import org.netbeans.junit.NbModuleSuite;
import org.netbeans.junit.NbTestCase;
import org.openide.util.Lookup;


public class ApplicationTest extends NbTestCase {
    
    //TODO: Update files for test modules.
    
    private static final String ADD_PLAYER_UPDATE_FILE = "C:\\Users\\Wolder\\Documents\\NetBeansProjects\\SolitudeWithoutResourceExtracter\\application\\src\\test\\java\\solitude\\forrestofsolitude\\PlayerOnlyXML.xml";
    private static final String REM_PLAYER_UPDATE_FILE = "C:\\Users\\Wolder\\Documents\\NetBeansProjects\\SolitudeWithoutResourceExtracter\\application\\src\\test\\java\\solitude\\forrestofsolitude\\NoModulesXML.xml";

    
    private static final String UPDATE_FILE = "C:\\Users\\Wolder\\Documents\\netbeans_site\\updates.xml";
    private static final String TEMP_UPDATEFILE = "C:\\Users\\Wolder\\Documents\\NetBeansProjects\\SolitudeWithoutResourceExtracter\\application\\src\\test\\java\\solitude\\forrestofsolitude\\tempUpdate.xml";

    

    public static Test suite() {
        return NbModuleSuite.createConfiguration(ApplicationTest.class).
                gui(false).
                failOnMessage(Level.WARNING). // works at least in RELEASE71
                failOnException(Level.INFO).
                enableClasspathModules(false). 
                clusters(".*").
                suite(); // RELEASE71+, else use NbModuleSuite.create(NbModuleSuite.createConfiguration(...))
    }

    public ApplicationTest(String n) {
        super(n);
    }

    public void testApplication() throws InterruptedException, IOException {
//        // Setup
//        List<InfcEntityUpdateService> update = new CopyOnWriteArrayList<>();
//        List<InfcEntityInstantiateService> instanciate = new CopyOnWriteArrayList<>();
//        waitForUpdate(update, instanciate);
//
//        // TEST: Load Player
//        copy(get(ADD_PLAYER_UPDATE_FILE), get(UPDATE_FILE), REPLACE_EXISTING);
//        waitForUpdate(update, instanciate);
//        
//        // Player Loaded
//        assertEquals("One UpdateService", 1, update.size());
//        assertEquals("One InstantiateService", 1, instanciate.size());
//        
//        // TEST: Load unloaded
//        copy(get(REM_PLAYER_UPDATE_FILE), get(UPDATE_FILE), REPLACE_EXISTING);
//        waitForUpdate(update, instanciate);
//        
//        // pre asserts
//        assertEquals("No UpdateService", 0, update.size());
//        assertEquals("No InstantiateService", 0, instanciate.size());
//        
//        // TEST: Reload update file
//        copy(get(TEMP_UPDATEFILE), get(UPDATE_FILE), REPLACE_EXISTING);
       
    }

//    private void waitForUpdate(List<InfcEntityUpdateService> update, List<InfcEntityInstantiateService> instanciate) throws InterruptedException {
//        Thread.sleep(10000);
//        
//        update.clear();
//        update.addAll(Lookup.getDefault().lookupAll(InfcEntityUpdateService.class));
//        
//        instanciate.clear();
//        instanciate.addAll(Lookup.getDefault().lookupAll(InfcEntityInstantiateService.class));
//    }
}
