/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.fogofwar;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.Fog;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */

@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})

public class FogController implements InfcEntityInstantiateService {

    @Override
    public void create(GameWorld gameworld, GameData gamedata) {
      TexturePath sprite = new TexturePath("shades/shade_1.png", FogController.class, TextureType.FOG_OG_WAR);  
      gameworld.addSprite(sprite);
      Fog fog_of_war = new Fog();
      gameworld.addFog(fog_of_war);
    }

    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
       for(Fog f : gameworld.getFog()){
           gameworld.removeFog(f);
       }
    }
    
}
