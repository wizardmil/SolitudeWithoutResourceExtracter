/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.level;

import java.util.Random;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.EntityDeadTree;
import solitude.common.entities.entity_instances.EntityNightBush;
import solitude.common.entities.entity_instances.EntityNightTree;
import solitude.common.entities.entity_instances.EntityNightTree2;
import solitude.common.entities.entity_instances.EntitySmallStone;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.Background;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})

public class MapController implements InfcEntityInstantiateService {

    Random random = new Random();

    @Override
    public void create(GameWorld gameworld, GameData gamedata) {

        TexturePath dead_tree_sprite = new TexturePath("map_textures/dead_tree.png", MapController.class, TextureType.DEAD_TREE);
        gameworld.addSprite(dead_tree_sprite);
        TexturePath night_tree_1_sprite = new TexturePath("map_textures/blue_tree.png", MapController.class, TextureType.NIGHT_TREE);
        gameworld.addSprite(night_tree_1_sprite);
        TexturePath night_tree_2_sprite = new TexturePath("map_textures/blue_tree_2.png", MapController.class, TextureType.NIGHT_TREE_2);
        gameworld.addSprite(night_tree_2_sprite);
        TexturePath night_bush_sprite = new TexturePath("map_textures/night_bush.png", MapController.class, TextureType.NIGHT_BUSH);
        gameworld.addSprite(night_bush_sprite);
        TexturePath small_stone_sprite = new TexturePath("map_textures/small_stone.png", MapController.class, TextureType.SMALL_STONE);
        gameworld.addSprite(small_stone_sprite);
        TexturePath back_gr_sprite = new TexturePath("map_textures/night_grass.png", MapController.class, TextureType.BACKGROUND);
        gameworld.addSprite(back_gr_sprite);
        
        
        Background background = new Background();
        background.setPosition(0, 0);
        gameworld.addBackground(background);

        for (int a = 0; a < GameWorld.AMOUNT_OF_TREES; a++) {
            Entity night_tree = new EntityNightTree();
            night_tree.setX(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            night_tree.setY(random.nextInt(gameworld.getWORLD_SIZE() - 100));
            night_tree.setRadiusBound(8);
            night_tree.setSizex(50);
            night_tree.setSizey(70);
            night_tree.setCollideX(night_tree.getX() + night_tree.getSizex() / 2);
            night_tree.setCollideY(night_tree.getY() + 7);
            gameworld.addEntity(night_tree);
        }
        for (int b = 0; b < GameWorld.AMOUNT_OF_TREES; b++) {
            Entity night_tree_2 = new EntityNightTree2();
            night_tree_2.setX(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            night_tree_2.setY(random.nextInt(gameworld.getWORLD_SIZE() - 100));
            night_tree_2.setRadiusBound(8);
            night_tree_2.setSizex(50);
            night_tree_2.setSizey(70);
            night_tree_2.setCollideX(night_tree_2.getX() + night_tree_2.getSizex() / 2);
            night_tree_2.setCollideY(night_tree_2.getY() + 7);
            gameworld.addEntity(night_tree_2);
        }
        for (int c = 0; c < GameWorld.AMOUNT_OF_DEADTREES; c++) {
            Entity dead_tree = new EntityDeadTree();
            dead_tree.setX(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            dead_tree.setY(random.nextInt(gameworld.getWORLD_SIZE() - 100));
            dead_tree.setRadiusBound(8);
            dead_tree.setSizex(50);
            dead_tree.setSizey(70);
            dead_tree.setCollideX(dead_tree.getX() + dead_tree.getSizex() / 2);
            dead_tree.setCollideY(dead_tree.getY() + 7);
            gameworld.addEntity(dead_tree);
        }
        for (int c = 0; c < GameWorld.AMOUNT_OF_BUSHES; c++) {
            Entity night_bush = new EntityNightBush();
            night_bush.setX(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            night_bush.setY(random.nextInt(gameworld.getWORLD_SIZE() - 100));
            night_bush.setRadiusBound(4);
            night_bush.setSizex(30);
            night_bush.setSizey(30);
            night_bush.setCollideX(night_bush.getX() + night_bush.getSizex() / 2);
            night_bush.setCollideY(night_bush.getY() + 4);
            gameworld.addEntity(night_bush);
        }
        for (int c = 0; c < GameWorld.AMOUNT_OF_STONES; c++) {
            Entity small_stone = new EntitySmallStone();
            small_stone.setX(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            small_stone.setY(random.nextInt(gameworld.getWORLD_SIZE() - 100));
            small_stone.setSizex(50);
            small_stone.setSizey(70);
            small_stone.setCollideX(small_stone.getX() + small_stone.getSizex() / 2);
            small_stone.setCollideY(small_stone.getY() + 7);
            gameworld.addEntity(small_stone);
        }
    }

    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
        for (Entity e : gameworld.getEntities()) {
            gameworld.removeEntity(e);
        }
        for (Background b : gameworld.getBackgrounds()) {
            System.out.println("Removing background... " + b);
            gameworld.removeBackground(b);
        }

    }

}
