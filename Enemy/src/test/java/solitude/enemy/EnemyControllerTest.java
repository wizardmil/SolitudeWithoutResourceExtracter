/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.enemy;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;

/**
 *
 * @author Sadik
 */
public class EnemyControllerTest {
    
    private GameWorld gameworld;
    private GameData gamedata;
    
    @Before
    public void setUp() {
        this.gameworld = new GameWorld();
        this.gamedata = new GameData();
    }
    
    public EnemyControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class EnemyController.
     */
    @Test
    public void testCreate() {
        System.out.println("enemy create");
        EnemyController instance = new EnemyController();
        
        Assert.assertEquals(0, gameworld.getAmountOfEnemies());
                instance.create(gameworld, gamedata);
        Assert.assertEquals(20, gameworld.getAmountOfEnemies());
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of dispose method, of class EnemyController.
     */
    @Test
    public void testDispose() {
        System.out.println("enemy dispose");
        EnemyController instance = new EnemyController();
        instance.create(gameworld, gamedata);
        Assert.assertEquals(20, gameworld.getAmountOfEnemies());
        instance.dispose(gameworld, gamedata);
        Assert.assertEquals(0, gameworld.getAmountOfEnemies());

        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
