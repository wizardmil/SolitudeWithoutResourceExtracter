/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.enemy;

import java.util.Random;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.EntityDirection;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;
import solitude.common.weapons.Weapon;

/**
 *
 * @author Bruger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})

public class EnemyController implements InfcEntityInstantiateService {

    Random random = new Random();

    @Override
    public void create(GameWorld gameworld, GameData gamedata) {
        TexturePath idle = new TexturePath("enemy_sprites/walking.png", EnemyController.class, TextureType.ENEMY_IDLE);
        gameworld.addSprite(idle);
        TexturePath attack = new TexturePath("enemy_sprites/attack.png", EnemyController.class, TextureType.ENEMY_ATTACKING);
        gameworld.addSprite(attack);
        
        for (int i = 0; i < GameWorld.AMOUNT_OF_GUARDS; i++) {
            Entity enemy = new EnemyEntity();
            enemy.setX(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            enemy.setY(random.nextInt(gameworld.getWORLD_SIZE() - 50));
            enemy.setSizex(50);
            enemy.setSizey(32);
            ((EnemyEntity) enemy).setHealth(100);
            ((EnemyEntity) enemy).setWeapon(new Weapon(5, 20, "Fists"));
            ((EnemyEntity) enemy).setHitboxRadius(12);
            ((EnemyEntity) enemy).setHitboxx(enemy.getX() + enemy.getSizex() / 2);
            ((EnemyEntity) enemy).setHitboxy(enemy.getY() + enemy.getSizey() / 2);
            ((EnemyEntity) enemy).setDead(false);
            enemy.setDirection(EntityDirection.RIGHT);
            gameworld.addEntity(enemy);
        }
    }

    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
        for (Entity e : gameworld.getEntities()) {
            if (e instanceof EnemyEntity) {
                gameworld.removeEntity(e);
            }
        }
    }

}
