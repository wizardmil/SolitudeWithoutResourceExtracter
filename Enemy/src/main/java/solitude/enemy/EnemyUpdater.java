/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.enemy;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;

/**
 *
 * @author Bruger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)
public class EnemyUpdater implements InfcEntityUpdateService {

    PlayerEntity pl = null;

    @Override
    public void update(GameWorld world, GameData data) {

        for (Entity e : world.getEntities()) {
            if (pl == null) {
                if (e instanceof PlayerEntity) {
                    pl = (PlayerEntity) e;
                }
            }

            if (e instanceof EnemyEntity) {

                EnemyEntity en = (EnemyEntity) e;
                en.setHitboxx(en.getX() + en.getSizex() / 2);
                en.setHitboxy(en.getY() + en.getSizey() / 2);
                en.setOpponent(pl);

                if (pl != null) {
                    float x1 = en.getHitboxx();
                    float y1 = en.getHitboxy();
                    float x2 = en.getOpponent().getHitboxx();
                    float y2 = en.getOpponent().getHitboxy();

                    double distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

                    if (distance < 120 && distance > 17) {
                        en.setTarget(true);
                        en.setAttacking(false);
                    } else if (distance < 18) {
                        en.setAttacking(true);
                        en.setTarget(false);
                    } else {
                        en.setAttacking(false);
                        en.setTarget(false);
                    }
                    en.wrap(world);
                    if (((EnemyEntity) e).getHealth() <= 0) {
                        en.setDead(true);
                        world.removeEntity(e);
                    }
                }
            }

        }

    }

}
