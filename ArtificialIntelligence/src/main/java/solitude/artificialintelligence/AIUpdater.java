/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.artificialintelligence;

import java.util.Random;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.EntityDirection;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;

/**
 *
 * @author Bruger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)
public class AIUpdater implements InfcEntityUpdateService {

    Random random = new Random();
    Random random2 = new Random();

    
    @Override
    public void update(GameWorld world, GameData data) {
        for (Entity e : world.getEntities()) {
            if (e instanceof EnemyEntity) {
                if (((EnemyEntity) e).isTarget()) {
                    chargeTarget((EnemyEntity) e, world, data);
                } else if (!((EnemyEntity) e).isAttacking()) {
                    walkWithoutPurpose((EnemyEntity) e, data);
                }
            }
        }
    }
    
    private void walkWithoutPurpose(EnemyEntity e, GameData data) {
            float x = e.getX();
            float y = e.getY();
            if(e.getMovingDuration() == 0) {
                e.setRandom(random.nextInt(101));
                e.setRandom2(random2.nextInt(100));
            }
            if(e.getRandom2() > 50){
                e.setWalk(20);
            }
            if(e.getRandom2() <= 50){
                e.setWalk(40);
            }
            if(e.getRandom()<= 25 && !e.isMovingRight() && !e.isMovingUp() && !e.isMovingDown()) {
                e.setMovingLeft(true);
                e.setDirection(EntityDirection.LEFT);
                e.setDx(-25);
                if(e.getRandom() > 12) e.setDy(0);
            }
            if(e.getRandom() > 25 && e.getRandom() <= 50 && !e.isMovingLeft() && !e.isMovingUp() && !e.isMovingDown()) {
                e.setMovingRight(true);
                e.setDirection(EntityDirection.RIGHT);
                e.setDx(25);
                if(e.getRandom() < 38) e.setDy(0);
            }
            if(e.getRandom() > 50 && e.getRandom() <= 75 && !e.isMovingLeft() && !e.isMovingRight() && !e.isMovingDown()) {
                e.setMovingUp(true);
                e.setDy(25);
                if(e.getRandom() < 63) e.setDx(0);
            }
            if(e.getRandom() > 75 && e.getRandom() <= 101 && !e.isMovingLeft() && !e.isMovingRight() && !e.isMovingUp()) {
                e.setMovingDown(true);
                e.setDy(-25);
                if(e.getRandom() > 87) e.setDx(0);
            }
            if(e.getMovingDuration() >= e.getWalk()){
                e.setMovingLeft(false);
                e.setMovingRight(false);
                e.setMovingUp(false);
                e.setMovingDown(false);
                e.setMovingDuration(0);
            }
            x += e.getDx() * data.getDelta();
            y += e.getDy() * data.getDelta();
            movingInDirection(e);
            e.setX(x);
            e.setY(y);
    }

    private void movingInDirection(EnemyEntity e) {
        if (e.isMovingLeft() || e.isMovingRight() || e.isMovingDown() || e.isMovingUp()) {
            int m = e.getMovingDuration();
            m += 1;
            e.setMovingDuration(m);
        }
    }

    private void chargeTarget(EnemyEntity enemy, GameWorld world, GameData data) {
        float xLocation = enemy.getX();
        float yLocation = enemy.getY();
        for (Entity p : world.getEntities()) {
            if (p instanceof PlayerEntity) {
                if (enemy.getX() < p.getX()) {
                    enemy.setDirection(EntityDirection.RIGHT);
                    xLocation += 40 * data.getDelta();
                }
                if (enemy.getX() > p.getX()) {
                    enemy.setDirection(EntityDirection.LEFT);
                    xLocation -= 40 * data.getDelta();
                }
                if (enemy.getY() < p.getY()) {
                    yLocation += 40 * data.getDelta();
                }
                if (enemy.getY() > p.getY()) {
                    yLocation -= 40 * data.getDelta();
                }
                    enemy.setY(yLocation);
                    enemy.setX(xLocation);
            }
        }
    }
    
}
