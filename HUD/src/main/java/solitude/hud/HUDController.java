/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.hud;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;
import solitude.common.statistics.HeadsUpDisplay;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})

public class HUDController implements InfcEntityInstantiateService{

    @Override
    public void create(GameWorld gameworld, GameData gamedata) {
        
        TexturePath sprite = new TexturePath("hud/hud.png", HUDController.class, TextureType.HUD);
        gameworld.addSprite(sprite);
        
        HeadsUpDisplay hud = new HeadsUpDisplay();
        hud.setPosition(70, 10);
        gameworld.addHeadsUpDisplay(hud);
    }

    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
      for(HeadsUpDisplay h : gameworld.getHeadsUpDisplay()){
          gameworld.removeHeadsUpDisplay(h);
      }
    }
    
}
