/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.player;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.EntityDirection;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})

public class PlayerController implements InfcEntityInstantiateService {
    
    @Override
    public void create(GameWorld gameworld, GameData gamedata) {
        TexturePath idle = new TexturePath("player_sprites/idle.png", PlayerController.class, TextureType.PLAYER_IDLE);
        gameworld.addSprite(idle);
        TexturePath walking = new TexturePath("player_sprites/running.png", PlayerController.class, TextureType.PLAYER_WALKING);
        gameworld.addSprite(walking);
        
        Entity player = new PlayerEntity();
        player.setDirection(EntityDirection.RIGHT);
        ((PlayerEntity) player).setIdle(true);
        ((PlayerEntity) player).setHitboxRadius(12);
        ((PlayerEntity) player).hasWeapon(false);
        ((PlayerEntity) player).setHealth(350);
        ((PlayerEntity) player).setDead(false);
        player.setRadiusBound(7);
        player.setSizex(50);
        player.setSizey(32);
        player.setX(100);
        player.setY(100);
        player.setCollideX(player.getX() + player.getSizex() / 2);
        player.setCollideY(player.getY() + 7);
        
        gameworld.addEntity(player);
    }
    
    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
        for (Entity e : gameworld.getEntities()) {
            if (e instanceof PlayerEntity) {
                System.out.println("Deleting the player...");
                ((PlayerEntity) e).setAttacking(false);
                ((PlayerEntity) e).setWalking(false);
                ((PlayerEntity) e).setIdle(false);
                gameworld.removeEntity(e);
            }
        }
    }
    
}
