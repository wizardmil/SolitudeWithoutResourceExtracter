/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.player;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.EntityDirection;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameKeys;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;

/**
 *
 * @author Bruger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)
public class PlayerUpdater implements InfcEntityUpdateService {

    @Override
    public void update(GameWorld world, GameData data) {
        for (Entity p : world.getEntities()) {
            if (p instanceof PlayerEntity) {
                float x = p.getX();
                float y = p.getY();

                ((PlayerEntity) p).setHitboxx(x + p.getSizex() / 2);
                ((PlayerEntity) p).setHitboxy(y + p.getSizey() / 2);
                p.setCollideX(p.getX() + p.getSizex() / 2);
                p.setCollideY(p.getY() + 7);

                if (data.getKeys().isDown(GameKeys.SPACE) && ((PlayerEntity) p).hasWeapon()) {
                    ((PlayerEntity) p).setAttacking(true);
                    ((PlayerEntity) p).setWalking(false);
                    ((PlayerEntity) p).setIdle(false);
                } else {
                    ((PlayerEntity) p).setAttacking(false);
                }

                if (!((PlayerEntity) p).isAttacking()) {
                    if (data.getKeys().isDown(GameKeys.RIGHT)) {
                        p.setDirection(EntityDirection.RIGHT);
                        x += 70 * data.getDelta();
                        ((PlayerEntity) p).setWalking(true);
                    }
                    if (data.getKeys().isDown(GameKeys.LEFT)) {
                        p.setDirection(EntityDirection.LEFT);
                        x += -70 * data.getDelta();
                        ((PlayerEntity) p).setWalking(true);
                    }
                    if (data.getKeys().isDown(GameKeys.UP)) {
                        y += 70 * data.getDelta();
                        ((PlayerEntity) p).setWalking(true);
                    }
                    if (data.getKeys().isDown(GameKeys.DOWN)) {
                        y += -70 * data.getDelta();
                        ((PlayerEntity) p).setWalking(true);
                    }
                }
                if (!((PlayerEntity) p).isAttacking() && !data.getKeys().isDown(GameKeys.UP)
                        && !data.getKeys().isDown(GameKeys.DOWN)
                        && !data.getKeys().isDown(GameKeys.LEFT)
                        && !data.getKeys().isDown(GameKeys.RIGHT)) {
                    ((PlayerEntity) p).setWalking(false);
                    ((PlayerEntity) p).setAttacking(false);
                    ((PlayerEntity) p).setIdle(true);
                } else {
                    ((PlayerEntity) p).setIdle(false);
                }

                p.setX(x);
                p.setY(y);
                ((PlayerEntity) p).wrap(world);

                if(((PlayerEntity) p).getHealth() <= 0){
                    ((PlayerEntity) p).setDead(true);
                    world.removeEntity(p);
                }
            }
        }
    }

}
