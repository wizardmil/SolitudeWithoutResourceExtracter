/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.game_data;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Wolder
 */
public class Background implements Serializable{
    private final UUID ID = UUID.randomUUID();
    
    private int x, y;
    
    public Background(){
        
    }

    public String getID() {
        return ID.toString();
    }
    
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public int getX(){
        return this.x;
    }
    
    public int getY(){
        return this.y;
    }
}
