/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.game_data;

import solitude.common.statistics.Statistics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import solitude.common.entities.Entity;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.statistics.HeadsUpDisplay;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class GameWorld {

    private final Map<String, Entity> worldEntities = new ConcurrentHashMap<>();
    private final Map<String, Background> backgrounds = new ConcurrentHashMap<>();
    private final Map<String, Fog> fog_of_war = new ConcurrentHashMap<>();
    private final Map<String, Statistics> statistics = new ConcurrentHashMap<>();
    private final Map<String, HeadsUpDisplay> heads_up_display = new ConcurrentHashMap<>();
    
    private final Map<String, TexturePath> sprites = new ConcurrentHashMap<>();

    private final int WORLD_SIZE = 1000;
    private final float WORLD_SCALE = 1.0f;

    public final static int AMOUNT_OF_GUARDS = 20;

    public final static int AMOUNT_OF_TREES = 50;
    public final static int AMOUNT_OF_DEADTREES = 50;
    public final static int AMOUNT_OF_BUSHES = 60;
    public final static int AMOUNT_OF_STONES = 100;

    public int getAmountOfEnemies() {
        ArrayList<EnemyEntity> al = new ArrayList<>();
        for (Entity e : getEntities()) {
            if (e instanceof EnemyEntity) {
                al.add((EnemyEntity) e);
            }
        }
        return al.size();
    }
    
    public Collection<TexturePath> getSprites() {
        synchronized (sprites) {
            return sprites.values();
        }
    }

    public void addSprite(TexturePath texture) {
        synchronized (sprites) {
            sprites.put(texture.getID(), texture);
        }
    }

    public void removeSprite(TexturePath texture) {
        synchronized (sprites) {
            sprites.remove(texture.getID());
        }
    }

    public Collection<HeadsUpDisplay> getHeadsUpDisplay() {
        synchronized (heads_up_display) {
            return heads_up_display.values();
        }
    }

    public void addHeadsUpDisplay(HeadsUpDisplay hud) {
        synchronized (heads_up_display) {
            heads_up_display.put(hud.getID(), hud);
        }
    }

    public void removeHeadsUpDisplay(HeadsUpDisplay hud) {
        synchronized (heads_up_display) {
            heads_up_display.remove(hud.getID());
        }
    }

    public Collection<Entity> getEntities() {
        synchronized (worldEntities) {
            return worldEntities.values();
        }
    }

    public void addEntity(Entity entity) {
        synchronized (worldEntities) {
            worldEntities.put(entity.getID(), entity);
        }
    }

    public void removeEntity(Entity entity) {
        synchronized (worldEntities) {
            worldEntities.remove(entity.getID());
        }
    }

    public Collection<Statistics> getStatistics() {
        synchronized (statistics) {
            return statistics.values();
        }
    }

    public void addStatistics(Statistics stats) {
        synchronized (statistics) {
            statistics.put(stats.getID(), stats);
        }
    }

    public void removeStatistics(Statistics stats) {
        synchronized (statistics) {
            statistics.remove(stats.getID());
        }
    }

    public Collection<Background> getBackgrounds() {
        synchronized (backgrounds) {
            return backgrounds.values();
        }
    }

    public void addBackground(Background background) {
        synchronized (backgrounds) {
            backgrounds.put(background.getID(), background);
        }
    }

    public void removeBackground(Background background) {
        synchronized (backgrounds) {
            backgrounds.remove(background.getID());
        }
    }

    public Collection<Fog> getFog() {
        synchronized (fog_of_war) {
            return fog_of_war.values();
        }
    }

    public void addFog(Fog foreground) {
        synchronized (fog_of_war) {
            fog_of_war.put(foreground.getID(), foreground);
        }
    }

    public void removeFog(Fog foreground) {
        synchronized (fog_of_war) {
            fog_of_war.remove(foreground.getID());
        }
    }

    public int getWORLD_SIZE() {
        return this.WORLD_SIZE;
    }

    public float getWORLD_SCALE() {
        return this.WORLD_SCALE;
    }

    private ArrayList<Entity> getAllEntitiesAsList() {
        ArrayList<Entity> al = new ArrayList<>();
        for (Entity e : getEntities()) {
            al.add(e);
        }
        return al;
    }

    public List<Entity> getSortetListOfEntities() {
        List<Entity> r = getAllEntitiesAsList();
        r.sort((Entity ent1, Entity ent2) -> {
            final float y1 = ent1.getY(), y2 = ent2.getY();
            if (y2 < y1) {
                return -1;
            } else if (y2 > y1) {
                return 1;
            } else {
                return 0;
            }
        });
        return r;
    }
}
