/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.game_data;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class GameData {
    
    private static int displayWidth;
    private static int displayHeight;

    
    private final GameKeys keys = new GameKeys();
    private float delta;
    
    public void setDelta(float delta){
        this.delta = delta;
    }
    
    public float getDelta(){
        return this.delta;
    }
    
    public GameKeys getKeys() {
        return keys;
    }

    public int getDisplayWidth() {
        return displayWidth;
    }

    public void setDisplayWidth(int displayWidth) {
        GameData.displayWidth = displayWidth;
    }

    public int getDisplayHeight() {
        return displayHeight;
    }

    public void setDisplayHeight(int displayHeight) {
        GameData.displayHeight = displayHeight;
    }
}
