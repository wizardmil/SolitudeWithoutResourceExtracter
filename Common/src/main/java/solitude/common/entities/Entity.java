/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.entities;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class Entity implements Serializable {
    
    private final UUID ID = UUID.randomUUID();
    
    protected float x;
    protected float y;
    
    private int sizex;
    private int sizey;
    
    private EntityDirection direction;
    
    private float collideX;
    private float collideY;
    
    private int radiusBound;
    
    public String getID(){
        return this.ID.toString();
    }

    public int getSizex() {
        return sizex;
    }

    public void setSizex(int sizex) {
        this.sizex = sizex;
    }

    public int getSizey() {
        return sizey;
    }

    public void setSizey(int sizey) {
        this.sizey = sizey;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public EntityDirection getDirection() {
        return direction;
    }

    public void setDirection(EntityDirection direction) {
        this.direction = direction;
    }

    public float getCollideX() {
        return collideX;
    }

    public void setCollideX(float collideX) {
        this.collideX = collideX;
    }

    public float getCollideY() {
        return collideY;
    }

    public void setCollideY(float collideY) {
        this.collideY = collideY;
    }

    public int getRadiusBound() {
        return radiusBound;
    }

    public void setRadiusBound(int radiusBound) {
        this.radiusBound = radiusBound;
    }
}
