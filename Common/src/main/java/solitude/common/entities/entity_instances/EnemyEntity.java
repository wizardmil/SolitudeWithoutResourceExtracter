/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.entities.entity_instances;

/**
 *
 * @author Bruger
 */
public class EnemyEntity extends CharacterEntity{
    
    private boolean attacking, target;
    private PlayerEntity opponent;
    
    private int random;
    private int moving_duration;
    private int walk;
    
    private int dx, dy;
    
    private boolean moving_right, moving_left, moving_up, moving_down;
    
    public EnemyEntity(){
        
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public int getWalk() {
        return walk;
    }

    public void setWalk(int walk) {
        this.walk = walk;
    }

    public int getRandom() {
        return random;
    }

    public void setRandom(int random) {
        this.random = random;
    }
    
    public int getRandom2() {
        return random;
    }

    public void setRandom2(int random) {
        this.random = random;
    }    

    public int getMovingDuration() {
        return moving_duration;
    }

    public void setMovingDuration(int moving_duration) {
        this.moving_duration = moving_duration;
    }

    public boolean isMovingRight() {
        return moving_right;
    }

    public void setMovingRight(boolean moving_right) {
        this.moving_right = moving_right;
    }

    public boolean isMovingLeft() {
        return moving_left;
    }

    public void setMovingLeft(boolean moving_left) {
        this.moving_left = moving_left;
    }

    public boolean isMovingUp() {
        return moving_up;
    }

    public void setMovingUp(boolean moving_up) {
        this.moving_up = moving_up;
    }

    public boolean isMovingDown() {
        return moving_down;
    }

    public void setMovingDown(boolean moving_down) {
        this.moving_down = moving_down;
    }

    public PlayerEntity getOpponent() {
        return opponent;
    }

    public void setOpponent(PlayerEntity opponent) {
        this.opponent = opponent;
    }

    public boolean isAttacking() {
        return attacking;
    }

    public void setAttacking(boolean attacking) {
        this.attacking = attacking;
    }

    public boolean isTarget() {
        return target;
    }

    public void setTarget(boolean target) {
        this.target = target;
    }
    
    
}
