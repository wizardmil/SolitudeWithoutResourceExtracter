/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.entities.entity_instances;

import solitude.common.entities.Entity;
import solitude.common.game_data.GameWorld;
import solitude.common.weapons.Weapon;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class CharacterEntity extends Entity{
    
    private float hitboxx, hitboxy, hitbox_radius;
    private int health;
    
    private Weapon weapon;
    private boolean dead;

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public float getHitboxRadius() {
        return hitbox_radius;
    }

    public void setHitboxRadius(float hitbox_radius) {
        this.hitbox_radius = hitbox_radius;
    }

    public float getHitboxx() {
        return hitboxx;
    }

    public void setHitboxx(float hitboxx) {
        this.hitboxx = hitboxx;
    }

    public float getHitboxy() {
        return hitboxy;
    }

    public void setHitboxy(float hiboxy) {
        this.hitboxy = hiboxy;
    }
    
    public void wrap(GameWorld world) {
        if (x < -20) {
            x = (world.getWORLD_SIZE() * world.getWORLD_SCALE()) - 20;
        }
        if (x > (world.getWORLD_SIZE() * world.getWORLD_SCALE()) - 20) {
            x = -20;
        }
        if (y < 0) {
            y = (world.getWORLD_SIZE() * world.getWORLD_SCALE());
        }
        if (y > (world.getWORLD_SIZE() * world.getWORLD_SCALE())) {
            y = 0;
        }
    }
}
