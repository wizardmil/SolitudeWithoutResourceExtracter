/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.entities.entity_instances;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class PlayerEntity extends CharacterEntity {
    
    private int hitBox;
    
    private boolean attacking = false, idle = false, walking = false, has_weapon = false;
    
    public PlayerEntity(){
        
    }

    public boolean hasWeapon() {
        return has_weapon;
    }

    public void hasWeapon(boolean has_weapon) {
        this.has_weapon = has_weapon;
    }
    
    public boolean isAttacking() {
        return attacking;
    }

    public void setAttacking(boolean attacking) {
        this.attacking = attacking;
    }

    public boolean isIdle() {
        return idle;
    }

    public void setIdle(boolean idle) {
        this.idle = idle;
    }

    public boolean isWalking() {
        return walking;
    }

    public void setWalking(boolean walking) {
        this.walking = walking;
    }
    
    
}
