/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.entity_textures;

/**
 *
 * @author Bruger
 */
public enum TextureType {
    PLAYER_ATTACK_1, PLAYER_ATTACK_2, ENEMY_IDLE, ENEMY_ATTACKING, PLAYER_WALKING, PLAYER_IDLE, 
    NIGHT_TREE, NIGHT_TREE_2, NIGHT_BUSH, SMALL_STONE, DEAD_TREE, SHADOW_BUSH, SHADOW_TREE_1, 
    SHADOW_TREE_2, HUD, FOG_OG_WAR, BACKGROUND;
}
