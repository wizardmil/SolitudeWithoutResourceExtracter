/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.statistics;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public class Statistics implements Serializable{
    private final UUID ID = UUID.randomUUID();
    
    private String title;
    private int x, y, counter;
    
    public Statistics(String title, int counter){
        this.title = title;
        this.counter = counter;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public void setCounter(int c){
        this.counter = c;
    }
    
    public int getCounter(){
        return counter;
    }
    
    public String getTitle(){
        return title;
    }

    public String getID() {
        return ID.toString();
    }
    
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public int getX(){
        return this.x;
    }
    
    public int getY(){
        return this.y;
    }
    
}
