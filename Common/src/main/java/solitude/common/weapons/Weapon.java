/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.weapons;

/**
 *
 * @author Bruger
 */
public class Weapon {
    private int damage;
    private String name;
    private int cooldown;

    public Weapon(int damage, int cooldown, String name){
        this.damage = damage;
        this.cooldown = cooldown;
        this.name = name;
    }
    
    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }
    
}
