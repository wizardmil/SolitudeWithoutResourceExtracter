/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.common.provided_interfaces;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
public interface InfcEntityInstantiateService {
    
    void create(solitude.common.game_data.GameWorld gameworld, solitude.common.game_data.GameData gamedata);
    
    void dispose(solitude.common.game_data.GameWorld gameworld, solitude.common.game_data.GameData gamedata);  
}
