/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.power;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.CharacterEntity;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.weapons.Weapon;

/**
 *
 * @author Sadik
 */
public class PowerUpdaterTest {
    GameWorld gameworld;
    GameData gamedata;
    public PowerUpdaterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.gamedata = new GameData();
        this.gameworld = new GameWorld();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of update method, of class PowerUpdater.
     */
    @org.junit.Test
    public void testUpdate() {
        System.out.println("power update");
        PowerUpdater instance = new PowerUpdater();
        PlayerEntity pl = new PlayerEntity();
        Assert.assertEquals(0, gameworld.getAmountOfEnemies());     
        Assert.assertFalse(pl.hasWeapon());
        for (int i = 0; i<16; i++){
            EnemyEntity x = new EnemyEntity();
            gameworld.addEntity(x);  
        }
        instance.update(gameworld, gamedata);
        
        Assert.assertEquals(16, gameworld.getAmountOfEnemies());
        for (Entity p : gameworld.getEntities()){
            if (p instanceof PlayerEntity){
                Assert.assertTrue(((PlayerEntity) p).hasWeapon());
            }
        }
        
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
