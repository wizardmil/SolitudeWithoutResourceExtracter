/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.power;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.entity_textures.TexturePath;
import solitude.common.entity_textures.TextureType;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;
import solitude.common.weapons.Weapon;

/**
 *
 * @author Bruger
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)
public class PowerUpdater implements InfcEntityUpdateService {
    private boolean loaded1 = false;
    private boolean loaded2 = false;

    @Override
    public void update(GameWorld world, GameData data) {
        for (Entity player : world.getEntities()) {
            if (player instanceof PlayerEntity) {
                if (world.getAmountOfEnemies() > 15 && !loaded1) {
                    TexturePath sprite = new TexturePath("player_sprites/attack.png", PowerUpdater.class, TextureType.PLAYER_ATTACK_1);
                    world.addSprite(sprite);
                    ((PlayerEntity) player).setWeapon(new Weapon(20, 20, "standard"));
                    ((PlayerEntity) player).hasWeapon(true);
                    loaded1 = true;
                }
                if (world.getAmountOfEnemies() <= 15 && !loaded2){
                    TexturePath sprite = new TexturePath("player_sprites/attack_2.png", PowerUpdater.class, TextureType.PLAYER_ATTACK_2);
                    world.addSprite(sprite);
                    ((PlayerEntity) player).setWeapon(new Weapon(50, 20, "rage"));
                    ((PlayerEntity) player).hasWeapon(true);
                    loaded2 = true;
                }
            }
        }
    }

}
