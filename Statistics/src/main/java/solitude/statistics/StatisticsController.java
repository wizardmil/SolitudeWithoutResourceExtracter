/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.statistics;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityInstantiateService;
import solitude.common.statistics.EnemyCounterStatistic;
import solitude.common.statistics.PlayerDamageStatistics;
import solitude.common.statistics.Statistics;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */

@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityInstantiateService.class)})
public class StatisticsController implements InfcEntityInstantiateService{

    @Override
    public void create(GameWorld gameworld, GameData gamedata) {
       Statistics enemy_counter = new EnemyCounterStatistic("Game score : ", gameworld.getAmountOfEnemies());
       enemy_counter.setPosition(155, 235);
       Statistics damage_stats = new PlayerDamageStatistics("Player weapon : ", 0);
       damage_stats.setPosition(155, 225);
       gameworld.addStatistics(enemy_counter);
       gameworld.addStatistics(damage_stats);
    }

    @Override
    public void dispose(GameWorld gameworld, GameData gamedata) {
       
    }
    
}
