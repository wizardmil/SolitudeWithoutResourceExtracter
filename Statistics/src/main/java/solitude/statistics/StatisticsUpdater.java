/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.statistics;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;
import solitude.common.statistics.EnemyCounterStatistic;
import solitude.common.statistics.PlayerDamageStatistics;
import solitude.common.statistics.Statistics;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)
public class StatisticsUpdater implements InfcEntityUpdateService {

    @Override
    public void update(GameWorld world, GameData data) {
        for (Statistics s : world.getStatistics()) {
            if (s instanceof EnemyCounterStatistic) {
                if (GameWorld.AMOUNT_OF_GUARDS > world.getAmountOfEnemies() && world.getAmountOfEnemies() > 0) {
                    s.setCounter((int) (60 * (GameWorld.AMOUNT_OF_GUARDS - world.getAmountOfEnemies()) - data.getDelta()));
                }
            }
            if (s instanceof PlayerDamageStatistics) {
                for (Entity e : world.getEntities()) {
                    if (e instanceof PlayerEntity) {
                        if (((PlayerEntity) e).getWeapon() != null) {
                            s.setTitle(((PlayerEntity) e).getWeapon().getName() + " ");
                            s.setCounter(((PlayerEntity) e).getWeapon().getDamage());
                        }
                    }
                }
            }
        }
    }

}
