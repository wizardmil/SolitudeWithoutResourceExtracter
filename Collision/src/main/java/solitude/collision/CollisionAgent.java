/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solitude.collision;

import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import solitude.common.entities.Entity;
import solitude.common.entities.entity_instances.EnemyEntity;
import solitude.common.entities.entity_instances.PlayerEntity;
import solitude.common.entities.entity_instances.WorldObject;
import solitude.common.game_data.GameData;
import solitude.common.game_data.GameWorld;
import solitude.common.provided_interfaces.InfcEntityUpdateService;

/**
 *
 * @author Emil Stubbe Kolvig-Raun
 */
@ServiceProviders(value = {
    @ServiceProvider(service = InfcEntityUpdateService.class)}
)

public class CollisionAgent implements InfcEntityUpdateService {

    float screenX;
    float screenY;
    boolean loadInformation = false;

    @Override
    public void update(GameWorld world, GameData data) {
        if (!loadInformation) {
            screenY = (((data.getDisplayHeight() / 2) / 1.5f) / 2);
            screenX = (((data.getDisplayWidth() / 2) / 1.5f) / 2);
            loadInformation = true;
        }

        for (Entity entity_0 : world.getEntities()) {
            for (Entity entity_1 : world.getEntities()) {
                if (entity_1 instanceof PlayerEntity) {
                    if (entity_0.getX() < screenX + 70 + entity_1.getX()
                            && entity_0.getX() > (-screenX - 70) + entity_1.getX()) {
                        if (entity_0.getY() < screenY + 70 + entity_1.getY()
                                && entity_0.getY() > (-screenY - 70) + entity_1.getY()) {
                            if (entity_0 instanceof WorldObject) {
                                collisionOverlap((WorldObject) entity_0, (PlayerEntity) entity_1);
                            }
                            if (entity_0 instanceof EnemyEntity) {
                                if (((PlayerEntity) entity_1).isAttacking()) {

                                    if (hitboundOverlap((EnemyEntity) entity_0, (PlayerEntity) entity_1)) {
                                        int substractHP = ((EnemyEntity) entity_0).getHealth() - ((PlayerEntity) entity_1).getWeapon().getDamage();
                                        ((EnemyEntity) entity_0).setHealth(substractHP);
                                    }

                                }
                                if (((EnemyEntity) entity_0).isAttacking()) {

                                    if (hitboundOverlap((EnemyEntity) entity_0, (PlayerEntity) entity_1)) {
                                        int substractHP = ((PlayerEntity) entity_1).getHealth() - ((EnemyEntity) entity_0).getWeapon().getDamage();
                                        ((PlayerEntity) entity_1).setHealth(substractHP);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean collisionOverlap(WorldObject entity_0, PlayerEntity entity_1) {
        float entity_0Radius = entity_0.getRadiusBound();
        float entity_1Radius = entity_1.getRadiusBound();
        float dx = (entity_0.getCollideX() + entity_0Radius / 2) - (entity_1.getCollideX() + entity_1Radius / 2);
        float dy = (entity_0.getCollideY() + entity_0Radius / 2) - (entity_1.getCollideY() + entity_1Radius / 2);

        boolean check = Math.sqrt((dx * dx) + (dy * dy)) <= (entity_1Radius + entity_0Radius);

        if (check) {
            entity_1.setX((entity_1.getX() - dx));
            entity_1.setY((entity_1.getY() - dy));
        }
        return check;
    }

    private boolean hitboundOverlap(EnemyEntity entity_0, PlayerEntity entity_1) {
        float entity_0_hitbox = entity_0.getHitboxRadius();
        float entity_1_hitbox = entity_1.getHitboxRadius();
        float dx = (entity_0.getHitboxx() + entity_0_hitbox / 2) - (entity_1.getHitboxx() + entity_1_hitbox / 2);
        float dy = (entity_0.getHitboxy() + entity_0_hitbox / 2) - (entity_1.getHitboxy() + entity_1_hitbox / 2);

        boolean check = Math.sqrt((dx * dx) + (dy * dy)) <= (entity_1_hitbox + entity_0_hitbox);

        return check;
    }

}
